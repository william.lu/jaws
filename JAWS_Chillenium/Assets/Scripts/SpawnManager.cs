﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField]
    List<Transform> spawnPoints;

    [SerializeField]
    GameObject spawnParticle;

    [SerializeField]
    float respawnTime = 2;

    IEnumerator KillThenRespawn(PlayerController player)
    {
        float timer = 0;

        Killed(player);

        while (timer < respawnTime)
        {
            timer += Time.deltaTime;
            yield return null;
        }


        Respawn(player);

        yield break;
    }

    void Killed(PlayerController player)
    {
        player.enabled = false;
        player.dead = true;
    }

    void Respawn(PlayerController player)
    {
        player.enabled = true;
        player.dead = false;

        List<Transform> playerGameObjs = new List<Transform>();

        for (int x = 0; x < GameManager.instance.players.Count; x++)
        {
            playerGameObjs.Add(GameManager.instance.players[x].gameObject.transform);
        }

        Vector3 averageDistance = Distance.GetAveragePos(playerGameObjs);

        Transform furthestSpawnPoint = Distance.GetFurthest(spawnPoints, averageDistance);


        Instantiate(spawnParticle, furthestSpawnPoint.transform.position, Quaternion.identity);
        player.transform.position = furthestSpawnPoint.position;
    }

    public void KillPlayer(PlayerController player)
    {
        StartCoroutine(KillThenRespawn(player));
    }

}
