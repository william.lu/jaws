﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    [SerializeField]
    int[] elimCount;
    [SerializeField]
    int scoreToWin = 5;

    public void Setup(int players)
    {
        elimCount = new int[players];
    }

    public void AddScore(int playerID, int scoreAmount)
    {
        elimCount[playerID] += scoreAmount;
        CheckForEndGame(playerID);
    }

    private void CheckForEndGame(int playerID)
    {
        if (elimCount[playerID] > scoreToWin)
        {
            GameManager.instance.EndGame();
        }
    }

}
