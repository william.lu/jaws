﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class InputReceiver : MonoBehaviour
{

    public TransformFluid[] playerStates;

    bool[] pressed;

    public enum Phase
    {
        waitForAPress,
        Rock,
        Paper,
        Scissors
    }

    public Phase currentPhase = Phase.waitForAPress;

    private void Awake()
    {
        //GameManager.instance.playerCount
        pressed = new bool[4];
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }

    void TutorialPromptCheck()
    {
        switch (currentPhase)
        {
            case Phase.waitForAPress:
                break;
            case Phase.Rock:
                CycleState(TransformState.Rock);
                break;
            case Phase.Paper:
                CycleState(TransformState.Paper);
                break;
            case Phase.Scissors:
                CycleState(TransformState.Scissors);
                break;


        }
    }

    void CycleState(TransformState state)
    {
        if (currentPhase.ToString() == state.ToString())
        {
            //change from 4 to gamemanager.playercount
            for (int i = 0; i < 4; i++)
            {
                if (playerStates[i].currentState != state)
                {
                    return;
                }
            }

            //cycle if everyone is a rock, and the current phase is rock
            currentPhase += 1;
        }
    }

    void WaitForAPress()
    {
        for (int i=0; i<4; i++)
        {
            if (XCI.GetButtonDown(XboxButton.A, (XboxController)i)){
                pressed[i] = true;
                //spawn the player
            }
        }

        foreach (bool playerPressed in pressed)
        {
            if (playerPressed == false)
            {
                return;
            }
        }

        currentPhase += 1;
    }
}
