﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TransformState { Rock, Paper, Scissors }

public class TransformFluid : MonoBehaviour
{
    public TransformState currentState;

    [SerializeField]
    float cooldownTime;

    float timer;

    private void Update()
    {
        timer += Time.deltaTime;
    }

    public void ChangeState(TransformState newState)
    {
        if (CanChange())
        {
            if (newState == currentState)
                return;

            currentState = newState;
            timer = 0;
        }
    }

    bool CanChange()
    {
        return (timer > cooldownTime);
    }

}
