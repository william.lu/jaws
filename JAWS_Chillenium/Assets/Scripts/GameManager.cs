﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    [SerializeField]
    int playerCount = 4;

    [SerializeField]
    List<PlayerController> players;

    [SerializeField]
    ScoreManager score;
    [SerializeField]
    SceneManagement sceneManager;


    [SerializeField]
    float matchLength;
    [SerializeField]
    float currentGameTime;

    bool gameStarted;

    #region Setup

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        DontDestroyOnLoad(this);
    }

    void Start()
    {
        StartGame();
    }

    public void StartGame()
    {
        ResetGame();
        gameStarted = true;
        score.Setup(playerCount);
    }

    public int PlayerSubscribe(PlayerController player)
    {
        players.Add(player);
        return players.Count - 1;
    }

    #endregion

    #region GameState

    public void EndGame()
    {
        gameStarted = false;
    }

    public void ResetGame()
    {
        currentGameTime = matchLength;
    }

    void Update ()
    {
        CompletionCheck();
    }

    void CompletionCheck()
    {
        if (gameStarted)
        {
            if (currentGameTime <= 0)
            {
                EndGame();
            }
            else
            {
                currentGameTime -= Time.deltaTime;
            }

        }
    }

    #endregion

    public void Showdown(PlayerController attacker, PlayerController defender)
    {
        if (ShowdownOutcome(attacker, defender).Equals(null))
        {
            return;
        }
        else
        {
            PlayerController[] winnerLoser = ShowdownOutcome(attacker, defender);
            PlayerController winner = winnerLoser[0];
            PlayerController loser = winnerLoser[1];
            
        }
    }

    PlayerController[] ShowdownOutcome(PlayerController attacker, PlayerController defender)
    {
        TransformState defenderState = defender.transState.currentState;

        switch (attacker.transState.currentState)
        {
            case (TransformState.Rock):
                if (defenderState == TransformState.Paper)
                {
                    return new PlayerController[2] { defender, attacker };
                }
                if (defenderState == TransformState.Scissors)
                {
                    return new PlayerController[2] { attacker, defender };
                }
                break;
            case (TransformState.Paper):
                if (defenderState == TransformState.Rock)
                {
                    return new PlayerController[2] { attacker, defender };
                }
                if (defenderState == TransformState.Scissors)
                {
                    return new PlayerController[2] { defender, attacker };
                }
                break;
            case (TransformState.Scissors):
                if (defenderState == TransformState.Rock)
                {
                    return new PlayerController[2] { defender, attacker };
                }
                if (defenderState == TransformState.Paper)
                {
                    return new PlayerController[2] { attacker, defender };
                }
                break;
        }

        return null;
    }

}
