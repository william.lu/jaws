﻿using System.Collections;
using System.Collections.Generic;
using XboxCtrlrInput;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public TransformFluid transState;

    public XboxController controller;


    StateAttack attackComponent;
    Rigidbody rbComponent;
    MotorMove moveMotor;

    Vector3 direction;

    public int playerNumber;

    public bool dead;

    private void Awake()
    {
        attackComponent = GetComponent<StateAttack>();
        transState = GetComponent<TransformFluid>();
        moveMotor = GetComponent<MotorMove>();
        rbComponent = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        playerNumber = GameManager.instance.PlayerSubscribe(this);
    }

    void Update ()
    {
        InputUpdate();
	}

    private void FixedUpdate()
    {
        moveMotor.MoveDirection(direction, rbComponent);
        moveMotor.LookDirection(direction, rbComponent);
    }


    #region INPUT

    void InputUpdate()
    {
        UpdateDirection();
        UpdateTransformation();
        UpdateAttack();
    }

    void UpdateAttack()
    {
        if (XCI.GetButtonDown(XboxButton.A, controller))
        {
            attackComponent.Attack(this);
        }
    }

    void UpdateTransformation()
    {
        if (XCI.GetButtonDown(XboxButton.Y, controller))
        {
            transState.ChangeState(TransformState.Rock);
        }
        if (XCI.GetButtonDown(XboxButton.X, controller))
        {
            transState.ChangeState(TransformState.Paper);
        }
        if (XCI.GetButtonDown(XboxButton.B, controller))
        {
            transState.ChangeState(TransformState.Scissors);
        }
    }

    void UpdateDirection()
    {
        Vector2 input = new Vector2(XCI.GetAxis(XboxAxis.LeftStickX, controller), XCI.GetAxis(XboxAxis.LeftStickY, controller));
        direction = Camera.main.transform.parent.TransformDirection(new Vector3(input.x, 0, input.y));
        direction.y = 0;
        direction = direction.normalized;
    }

    #endregion

    public void Attacked(PlayerController attackType)
    {
        GameManager.instance.Showdown(attackType, this);
    }
}
