﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    TextMeshProUGUI TutorialPrompt;
    InputReceiver inputReceiver;

    Dictionary<GameObject, Vector3> UIScales;
    Dictionary<GameObject, Color> UIColors;


    private void Awake()
    {
        UIScales = new Dictionary<GameObject, Vector3>();
        UIColors = new Dictionary<GameObject, Color>();
        //ghetto af but make sure "tutorialPrompt" is the first child
        TutorialPrompt = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
    }

    // Use this for initialization
    void Start()
    {
        StartCoroutine(TutorialSequence());

    }


    // Update is called once per frame
    void Update()
    {
    }

    IEnumerator TutorialSequence()
    {
        float scaleDownTime = 0.7f;

        //INTRO
        ChangeTo("", TutorialPrompt);
        Hover(TutorialPrompt);
        yield return new WaitForSeconds(3f);
        StopHover(TutorialPrompt);
        scaleDownUI(TutorialPrompt, scaleDownTime);
        yield return new WaitForSeconds(scaleDownTime);

        //ROCK
        ChangeTo("press <color=#ff0000ff>B</color> to become a rock!", TutorialPrompt);
        Hover(TutorialPrompt);
        //WAIT UNTIL EVERYONE PRESSES B
        while (inputReceiver.currentPhase == InputReceiver.Phase.Rock)
        {
            yield return null;
        }
        StopHover(TutorialPrompt);
        scaleDownUI(TutorialPrompt, scaleDownTime);


    }

    void ChangeTo(string targetText, TextMeshProUGUI text)
    {
        if (targetText != "")
        {
            //only change if text is different than before
            text.text = targetText;
        }
        //reset text colour
        if (UIColors.ContainsKey(text.gameObject))
        {
            text.color = UIColors[text.gameObject];
        }
        //rotate slightly
        text.transform.DOPunchRotation(new Vector3(0, 0, Mathf.Abs(Random.Range(-3f, 3f))), 1f, 1).SetEase(Ease.OutBack);
        //scale up
        if (!UIScales.ContainsKey(text.gameObject))
        {
            UIScales.Add(text.gameObject, text.transform.localScale);
        }
        TutorialPrompt.transform.localScale = Vector3.zero;
        TutorialPrompt.transform.DOScale(UIScales[text.gameObject], 1).SetEase(Ease.OutBack);
    }

    void Hover(TextMeshProUGUI text)
    {
        if (UIScales.ContainsKey(text.gameObject))
            text.transform.DOScale(UIScales[text.gameObject] * 1.04f, 1.5f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo).SetDelay(1);
    }
    void StopHover(TextMeshProUGUI text)
    {
        text.transform.DOKill();
    }

    void scaleDownUI(TextMeshProUGUI text, float time)
    {

        text.transform.DOScale(Vector3.zero, time).SetEase(Ease.InCubic);

        if (!UIColors.ContainsKey(text.gameObject))
        {
            UIColors[text.gameObject] = text.color;
        }

        text.DOFade(0, time);
    }
}
