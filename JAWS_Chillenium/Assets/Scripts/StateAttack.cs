﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateAttack : MonoBehaviour
{
    [SerializeField]
    LayerMask layerAttackable;

    public void Attack(PlayerController myself)
    {
        Collider[] hit = CastAttack();

        for (int i = 0; i < hit.Length; i++)
        {
            hit[i].GetComponent<PlayerController>().Attacked(myself);
        }
    }

    Collider[] CastAttack()
    {
        return Physics.OverlapBox(transform.position + transform.forward, new Vector3(0.3F, 0.3F, 0.3F), transform.rotation, layerAttackable);
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube (transform.position + transform.forward, new Vector3(0.5F, 0.5F, 0.5F));
    }
#endif

}
